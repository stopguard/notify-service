from rest_framework import serializers
from rest_framework.relations import StringRelatedField
from rest_framework.serializers import ModelSerializer

from appclients.serializers import ClientSerializer
from appnotifications.models import Notify, Message


class NotifySerializer(ModelSerializer):
    """Сериализатор рассылок для всех запросов кроме GET"""
    def validate(self, data):
        """Валидация дат - stop должен быть больше чем start"""
        if data['start'] >= data['stop']:
            raise serializers.ValidationError('stop must occur after start')
        return data

    class Meta:
        model = Notify
        fields = ['id', 'start', 'stop', 'message', 'code_filter', 'tag_filter', ]


class NotifyStatsSerializer(ModelSerializer):
    """Сериализатор рассылок для ListView"""
    class Meta:
        model = Notify
        fields = ['id', 'start', 'stop', 'message',
                  'code_filter', 'tag_filter', 'messages_stats', ]


class MessageSerializer(ModelSerializer):
    """Сериализатор сообщений для ListView"""
    client = StringRelatedField()

    class Meta:
        model = Message
        fields = ['id', 'added', 'verbose_status', 'client']


class MessageDetailSerializer(ModelSerializer):
    """Сериализатор сообщений для RetrieveView"""
    client = ClientSerializer()
    notify = NotifySerializer()

    class Meta:
        model = Message
        fields = ['id', 'added', 'verbose_status', 'notify', 'client', ]


class NotifyDetailSerializer(ModelSerializer):
    """Сериализатор рассылок для RetrieveView"""
    messages = MessageSerializer(many=True)
    
    class Meta:
        model = Notify
        fields = ['id', 'start', 'stop', 'message',
                  'code_filter', 'tag_filter', 'messages_stats', 'messages']
