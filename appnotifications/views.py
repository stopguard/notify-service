from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework import viewsets, mixins
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from appnotifications.models import Message, Notify
from appnotifications.serializers import MessageSerializer, MessageDetailSerializer, \
    NotifySerializer, NotifyStatsSerializer, NotifyDetailSerializer
from notifyservice.tasks import create_messages, convert_time


class MessageViewSet(viewsets.ViewSet):
    """Ограничиваем ViewSet содержимым ТЗ"""
    def list(self, request):
        messages = Message.objects.all()
        serializer = MessageSerializer(messages, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        message = get_object_or_404(Message, pk=pk)
        serializer = MessageDetailSerializer(message)
        return Response(serializer.data)


class NotifyListModelMixin:
    """Скорректированный ListModelMixin"""
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = NotifyStatsSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = NotifyStatsSerializer(queryset, many=True)
        return Response(serializer.data)


class NotifyRetrieveModelMixin:
    """Скорректированный RetrieveModelMixin"""
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = NotifyDetailSerializer(instance)
        return Response(serializer.data)


class NotifyViewSet(mixins.CreateModelMixin,
                    NotifyRetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    NotifyListModelMixin,
                    GenericViewSet):
    queryset = Notify.objects.all()
    serializer_class = NotifySerializer


@receiver(post_save, sender=Notify)
def save_notify(sender, instance, **kwargs):
    """Сигнал создающий задачу в Celery при сохранении модели рассылки"""
    updated = convert_time(instance.updated)
    start = convert_time(instance.start)
    create_messages.apply_async((instance.id, updated,), eta=start)
