import pytz
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from appclients.models import Client


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class ClientPostSerializer(ModelSerializer):
    def validate_phone(self, value):
        """Базовая валидация номера телефона"""
        if not 70000000000 <= value < 80000000000:
            raise serializers.ValidationError('Incorrect phone number')
        return value

    def validate_timezone(self, value):
        """Валидация часового пояса"""
        if value not in pytz.all_timezones:
            raise serializers.ValidationError('Incorrect timezone number')
        return value

    class Meta:
        model = Client
        exclude = ['code']

