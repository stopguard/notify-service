from django.db import models

from notifyservice.settings import TIME_ZONE


class Client(models.Model):
    name = models.CharField('Имя', max_length=128)
    phone = models.PositiveBigIntegerField('Номер телефона')
    code = models.PositiveSmallIntegerField('Код мобильного оператора', db_index=True)
    tag = models.CharField('Тег', max_length=64, db_index=True)
    timezone = models.CharField('Часовой пояс', max_length=32, default=TIME_ZONE)

    @classmethod
    def get_client_set(cls, code_filter, tag_filter):
        """Возвращает QuerySet с клиентами подходящими под фильтры рассылки"""
        kwargs = {}
        if code_filter > 0:
            kwargs['code'] = code_filter
        if len(tag_filter) > 0:
            kwargs['tag'] = tag_filter
        if len(kwargs) > 0:
            return cls.objects.filter(**kwargs)
        return cls.objects.all()

    def save(self, force_insert=False, force_update=False,
             using=None, update_fields=None):
        """Автоматическое заполнение поля кода телефонного номера"""
        self.code = self.phone // 10000000 % 1000
        super().save(force_insert=force_insert, force_update=force_update,
                     using=using, update_fields=update_fields)

    def __str__(self):
        return f'{self.name} ({self.phone})'
