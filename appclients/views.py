from rest_framework.viewsets import ModelViewSet

from appclients.models import Client
from appclients.serializers import ClientSerializer, ClientPostSerializer


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()

    def get_serializer_class(self):
        """Определяем требующийся сериализатор по типу запроса"""
        if self.request.method in ['POST', 'PUT', 'PATCH']:
            return ClientPostSerializer
        return ClientSerializer
