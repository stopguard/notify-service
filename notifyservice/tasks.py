from datetime import datetime

import requests
from celery import group

from appclients.models import Client
from appnotifications.models import Notify, Message
from notifyservice import celery_app
from notifyservice.convert_time import convert_time, str_to_datetime, get_tz
from notifyservice.settings import SEND_SERVICE_API_JWT

send_service_api = 'https://probe.fbrq.cloud/v1'


@celery_app.task
def create_messages(notify_id, upd_time):
    """
    Отложенная задача создающая задачи отправки сообщений через внешний API

    notify_id -- идентификатор рассылки
    upd_time -- дата обновления рассылки на момент создания задачи
    """
    upd_time = str_to_datetime(upd_time)
    notify = Notify.objects.filter(id=notify_id).first()

    # Условия прекращения выполнения задачи:
    if notify is None:  # Если рассылка была удалена
        return f'notify id{notify_id} not found'
    if convert_time(notify.updated) > upd_time:  # Если рассылка была изменена после создания задачи
        return f'Notify id{notify_id} has updated. This task canceled'
    if datetime.now(get_tz()) > convert_time(notify.stop):  # Если пройдена дата прекращения рассылки
        return f'Notify id{notify_id} completion date has passed. This task canceled'

    # Получение списка клиентов по фильтрам и создание сообщений
    clients = Client.get_client_set(notify.code_filter, notify.tag_filter).only('id')
    created_messages = Message.objects.bulk_create(
        [Message(notify=notify, client=client) for client in clients]
    )

    try:
        group(send_message.delay(msg.id, upd_time) for msg in created_messages)()
        return f'Notification id{notify_id} tasks created'
    except Exception as e:
        return f'Notification id{notify_id} tasks is not created. Error: {e}'


@celery_app.task(bind=True, default_retry_delay=600, max_retries=None)
def send_message(self, message_id, upd_time):
    """
    Задача отправляющая сообщения через внешний API

    message_id -- идентификатор сообщения
    upd_time -- дата обновления рассылки на момент создания задачи
    """

    upd_time = str_to_datetime(upd_time)
    msg = Message.objects.filter(id=message_id).select_related().first()

    # Условия прекращения выполнения задачи
    if msg is None:     # Если сообщение было удалено
        return f'Message id{message_id} not found'
    if convert_time(msg.notify.updated) > upd_time:     # Если рассылка была изменена после создания задачи
        msg.status = Message.ABORTED
        msg.save()
        return f'Notify id{msg.notify_id} has updated. This task canceled'
    if datetime.now(get_tz()) > convert_time(msg.notify.stop):  # Если пройдена дата прекращения рассылки
        msg.status = Message.ERROR
        msg.save()
        return f'The time for sending the message id{message_id} has expired'

    try:
        response = requests.post(
            f'{send_service_api}/send/{message_id}',
            json={'id': message_id,
                  'phone': msg.client.phone,
                  'text': msg.notify.message},
            headers={'Content-Type': 'application/json;charset=utf-8',
                     'X-Requested-With': 'XMLHttpRequest',
                     'Authorization': f'Bearer {SEND_SERVICE_API_JWT}'}
        )

        result_message = response.text
        result_code = response.status_code
    except Exception as e:
        msg.status = Message.RESENDING
        msg.save()
        raise self.retry(exc=e)
    if result_code != 200:
        msg.status = Message.RESENDING
        msg.save()
        raise self.retry(exc=result_message or f"Unknown error. Status code {result_code}")
    msg.status = Message.SENT
    msg.save()
    return result_message
