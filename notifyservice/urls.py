from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from rest_framework.routers import DefaultRouter
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from appclients.views import ClientViewSet
from appnotifications.views import NotifyViewSet, MessageViewSet

# Schema для OpenAPI
schema_view = get_schema_view(
    openapi.Info(
        title='Notifyservice',
        default_version='0.0a',
        description='Documentation',
        contact=openapi.Contact(email='mertviy.rasras@gmail.com'),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

# Router для DRF
router = DefaultRouter()
router.register('clients', ClientViewSet)
router.register('notifications', NotifyViewSet)
router.register('messages', MessageViewSet, basename='messages')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
